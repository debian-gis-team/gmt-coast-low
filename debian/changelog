gmt-coast-low (1:2.1.1-1) unstable; urgency=low

  * New upstream release for GMT 4.5.6+
  * Moved to git repository, with Vcs-* fields update in debian/control.
  * Policy bumped to 3.9.1, no changes required.
  * Debhelper level now set to 8.
  * Source format set to 3.0.
  * Updated download script to add current GSHHS version.

 -- Francesco Paolo Lovergine <frankie@debian.org>  Mon, 28 Mar 2011 13:47:17 +0200

gmt-coast-low (1:2.0.2-1) unstable; urgency=low

  * New upstream release for GMT 4.5.2+
    (closes: #569585)
  * Set Section to misc as in override file.
  * Removed superfluous bash dependency (for helper script).
  * Policy bumped to 3.8.4 without changes.
  * Added a README.Debian file which predates NEWS and previous README.
    (closes: #555987)
  * Force ownership of files at tar extraction in download script.
    (closes: #569584)
  * Now download script allows also version choice.
  * Added strict version dependency in debian/control on gmt >= 4.5.2.

 -- Francesco Paolo Lovergine <frankie@debian.org>  Wed, 17 Feb 2010 10:07:55 +0100

gmt-coast-low (1:1.10-2) unstable; urgency=low

  * Fixed svn addresses in Vcs-* fields.
  * Fixed long description.
    (closes: #521061)
  * Now install gmt-coastline-download script.
  * Policy bumped to 3.8.1.
  * Debhelper level set to 7.

 -- Francesco Paolo Lovergine <frankie@debian.org>  Thu, 26 Mar 2009 00:03:47 +0100

gmt-coast-low (1:1.10-1) unstable; urgency=low

  * Now maintainership set to DebianGis team.
  * New long due upstream version, using epoch because upstream now has
    a proper versioning.
  * Added me as an uploader.
  * Policy bumped to 3.7.3.
  * Added Homepage and Vcs-* fields in debian/control.
  * Revised debian/control and debhelper compatibility moved to 5.

 -- Francesco Paolo Lovergine <frankie@debian.org>  Fri, 13 Jun 2008 14:20:12 +0200

gmt-coast-low (20020411-1.1) unstable; urgency=high

  * Non-maintainer upload.
  * Rebuild package to finish /usr/doc transition (Closes: #359415).
  * Move debhelper from build-depends-indep to build-depends.

 -- Thijs Kinkhorst <thijs@debian.org>  Sun,  4 Mar 2007 18:19:41 +0100

gmt-coast-low (20020411-1) unstable; urgency=high

  * Got the current coastline data and update the package to move it into 
    the package pool. This should stop the removal from woody.
  * debian/rules: Use --bzip2 instead of -I so it builds using both 
    old and new tar.
  * debian/control: Update to current policy (lintian)
    + Add build dependency for debhelper and bzip2.

 -- Torsten Landschoff <torsten@debian.org>  Thu, 11 Apr 2002 00:25:00 +0200

gmt-coast-low (19991001-3) frozen unstable; urgency=low

  * This is a one-line change and can go into frozen. It is to adjust 
    for the change in gmt - the coastline data has moved. The current
    GMT package is still compatible with the old coastline data but
    this is the path where it actually belongs. 
  
  * debian/rules: Install the datafiles into /usr/share/gmt instead of 
    /usr/share/gmt/lib.

 -- Torsten Landschoff <torsten@debian.org>  Fri, 21 Jan 2000 15:16:58 +0100

gmt-coast-low (19991001-2) unstable; urgency=low

  * debian/control: Changed description to reflect the current state 
    regarding coastline data in the distribution.
  * debian/control: Moved into science section.

 -- Torsten Landschoff <torsten@debian.org>  Fri, 14 Jan 2000 19:35:17 +0100

gmt-coast-low (19991001-1) unstable; urgency=low

  * Initial release.

 -- Torsten Landschoff <torsten@debian.org>  Fri,  1 Oct 1999 19:40:34 +0200
